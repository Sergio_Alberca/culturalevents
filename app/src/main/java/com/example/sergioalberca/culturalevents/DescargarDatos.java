package com.example.sergioalberca.culturalevents;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Sergio Alberca on 25/10/2016.
 */

public class DescargarDatos extends AsyncTask<String, String, String> {


    HttpURLConnection urlConnection;
    String id;
    Context context;
    ProgressBar pb;

    public DescargarDatos(String id,Context context, ProgressBar pb) {
        this.id = id;
        this.context = context;
        this.pb = pb;

    }

    protected void onPreExecute() {
        pb.setVisibility(View.VISIBLE);
    }


    @Override
    protected String doInBackground(String... args) {

        StringBuilder result = new StringBuilder();


        try {

            URL url = new URL("http://sergioalberca.hol.es/api/v1/acontecimiento/" + id);
            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            JSONObject raizAcontecimiento = new JSONObject(result.toString());
            DatosBBDD creacionBD = new DatosBBDD(context, Environment.getExternalStorageDirectory() + "/CulturalEvents.db", null, 1);
            //Le daremos permiso a la Base de Datos para que pueda escribir
            SQLiteDatabase db = creacionBD.getWritableDatabase();
            if(raizAcontecimiento.has("acontecimiento")) {
                MyLog.e("tag", "contiene acontecimiento");
                JSONObject objetoJsonInterno = new JSONObject(raizAcontecimiento.getString("acontecimiento"));

                if (db != null) {
                   //Comprobamos todos los campos del acontecimiento
                    String nombreAcontecimiento = (objetoJsonInterno.has("nombre")) ? objetoJsonInterno.getString("nombre") : "";
                    String nombreOrganizador = (objetoJsonInterno.has("organizador")) ? objetoJsonInterno.getString("organizador") : "";
                    String descripcion = (objetoJsonInterno.has("descripcion")) ? objetoJsonInterno.getString("descripcion") : "";
                    String tipo = (objetoJsonInterno.has("tipo")) ? objetoJsonInterno.getString("tipo") : "";
                    String portada = (objetoJsonInterno.has("portada")) ? objetoJsonInterno.getString("portada") : "";
                    String inicio = (objetoJsonInterno.has("inicio")) ? objetoJsonInterno.getString("inicio") : "";
                    String fin = (objetoJsonInterno.has("fin")) ? objetoJsonInterno.getString("fin") : "";
                    String direccion = (objetoJsonInterno.has("direccion")) ? objetoJsonInterno.getString("direccion") : "";
                    String localidad = (objetoJsonInterno.has("localidad")) ? objetoJsonInterno.getString("localidad") : "";
                    String cod_postal = (objetoJsonInterno.has("cod_postal")) ? objetoJsonInterno.getString("cod_postal") : "";
                    String provincia = (objetoJsonInterno.has("provincia")) ? objetoJsonInterno.getString("provincia") : "";
                    String latitud = (objetoJsonInterno.has("latitud")) ? objetoJsonInterno.getString("latitud") : "";
                    String longitud = (objetoJsonInterno.has("longitud")) ? objetoJsonInterno.getString("longitud") : "";
                    String telefono = (objetoJsonInterno.has("telefono")) ? objetoJsonInterno.getString("telefono") : "";
                    String email = (objetoJsonInterno.has("email")) ? objetoJsonInterno.getString("email") : "";
                    String web = (objetoJsonInterno.has("web")) ? objetoJsonInterno.getString("web") : "";
                    String facebook = (objetoJsonInterno.has("facebook")) ? objetoJsonInterno.getString("facebook") : "";
                    String twitter = (objetoJsonInterno.has("twitter")) ? objetoJsonInterno.getString("twitter") : "";
                    String instagram = (objetoJsonInterno.has("instagram")) ? objetoJsonInterno.getString("instagram") : "";

                    //Insertamos el acontecimiento en la base de datos
                    ContentValues nuevoRegistro = new ContentValues();
                    //El id es el que introduciremos en el movil
                    nuevoRegistro.put("id", id);
                    nuevoRegistro.put("nombre", nombreAcontecimiento);
                    nuevoRegistro.put("organizador", nombreOrganizador);
                    nuevoRegistro.put("descripcion", descripcion);
                    nuevoRegistro.put("tipo", tipo);
                    nuevoRegistro.put("portada", portada);
                    nuevoRegistro.put("inicio", inicio);
                    nuevoRegistro.put("fin", fin);
                    nuevoRegistro.put("direccion", direccion);
                    nuevoRegistro.put("localidad", localidad);
                    nuevoRegistro.put("cod_postal", cod_postal);
                    nuevoRegistro.put("provincia", provincia);
                    nuevoRegistro.put("latitud", latitud);
                    nuevoRegistro.put("longitud", longitud);
                    nuevoRegistro.put("telefono", telefono);
                    nuevoRegistro.put("email", email);
                    nuevoRegistro.put("web", web);
                    nuevoRegistro.put("facebook", facebook);
                    nuevoRegistro.put("twitter", twitter);
                    nuevoRegistro.put("instagram", instagram);
                    db.delete("acontecimiento","id="+ id,null);
                    db.insert("acontecimiento", null, nuevoRegistro);


                    if (raizAcontecimiento.has("evento")) {
                        //Creo un array de la lista de los eventos
                        JSONArray listaeventos = new JSONArray(raizAcontecimiento.getString("evento"));

                        for (int i = 0; i < listaeventos.length(); i++) {
                            JSONObject objetoJsonevento = (JSONObject) listaeventos.get(i);
                            MyLog.e("tag", "contiene evento");
                            //Comprobamos todos los campos del evento
                            String IdEvento = (objetoJsonevento.has("id")) ? objetoJsonevento.getString("id") : "";
                            String nombreAcontecimientoEvento = (objetoJsonevento.has("nombre")) ? objetoJsonevento.getString("nombre") : "";
                            String descripcionEvento = (objetoJsonevento.has("descripcion")) ? objetoJsonevento.getString("descripcion") : "";
                            String inicioEvento = (objetoJsonevento.has("inicio")) ? objetoJsonevento.getString("inicio") : "";
                            String finEvento = (objetoJsonevento.has("fin")) ? objetoJsonevento.getString("fin") : "";
                            String direccionEvento = (objetoJsonevento.has("direccion")) ? objetoJsonevento.getString("direccion") : "";
                            String localidadEvento = (objetoJsonevento.has("localidad")) ? objetoJsonevento.getString("localidad") : "";
                            String cod_postalEvento = (objetoJsonevento.has("cod_postal")) ? objetoJsonevento.getString("cod_postal") : "";
                            String provinciaEvento = (objetoJsonevento.has("provincia")) ? objetoJsonevento.getString("provincia") : "";
                            String latitudEvento = (objetoJsonevento.has("latitud")) ? objetoJsonevento.getString("latitud") : "";
                            String longitudEvento = (objetoJsonevento.has("longitud")) ? objetoJsonevento.getString("longitud") : "";

                            //Inserto los datos del evento
                            ContentValues nuevoRegistroEvento = new ContentValues();
                            nuevoRegistroEvento.put("id", IdEvento);
                            nuevoRegistroEvento.put("id_acontecimiento", id);
                            nuevoRegistroEvento.put("nombre", nombreAcontecimientoEvento);
                            nuevoRegistroEvento.put("descripcion", descripcionEvento);
                            nuevoRegistroEvento.put("inicio", inicioEvento);
                            nuevoRegistroEvento.put("fin", finEvento);
                            nuevoRegistroEvento.put("direccion", direccionEvento);
                            nuevoRegistroEvento.put("localidad", localidadEvento);
                            nuevoRegistroEvento.put("cod_postal", cod_postalEvento);
                            nuevoRegistroEvento.put("provincia", provinciaEvento);
                            nuevoRegistroEvento.put("latitud", latitudEvento);
                            nuevoRegistroEvento.put("longitud", longitudEvento);
                            db.insert("evento", null, nuevoRegistroEvento);

                        }
                        db.close();
                    }else{
                        MyLog.e("tag", "error evento");
                    }
                }
            }else{
                MyLog.e("tag", "error acontecimiento");
                return "0";
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        return result.toString();

    }

    @Override
    protected void onPostExecute(String result) {
        pb.setVisibility(View.INVISIBLE);
        if(result == "0") {
            Alerta("Error","El acontecimiento no existe");
        }else{
            SharedPreferences prefs = context.getSharedPreferences("Preferences",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("id", id);
            editor.commit();

            Intent intent = new Intent(context, MostrarAcontecimientosActivity.class);
            context.startActivity(intent);
            ((Activity)context).finish();
        }
    }

    public void Alerta(String titulo, String mensaje){
        AlertDialog.Builder alerta= new AlertDialog.Builder(context);
        alerta.setTitle(titulo);
        alerta.setMessage(mensaje);
        alerta.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alerta.show();
    }


    }





