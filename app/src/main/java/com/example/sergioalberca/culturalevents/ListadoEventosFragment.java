package com.example.sergioalberca.culturalevents;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListadoEventosFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListadoEventosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListadoEventosFragment extends ListFragment {
    private ListView listView;
    private ArrayList<EventoItem> items;
    private EventoAdapter EventoAdapter;
    private FrameLayout layoutPrincipal;
    private Context context;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ListadoEventosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListadoEventosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListadoEventosFragment newInstance(String param1, String param2) {
        ListadoEventosFragment fragment = new ListadoEventosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyLog.d("listadoFragment", "onCreate...");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MyLog.d("listadoFragment", "onCreateView...");

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_listado, container, false);

        View rootView = inflater.inflate(R.layout.fragment_listado_eventos, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //int num = getArguments().getInt(ARG_SECTION_NUMBER);

        // We need to use a different list item layout for devices older than Honeycomb
        int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;

        items = new ArrayList<EventoItem>();
        RecogerDatos();
        EventoAdapter = new EventoAdapter(getActivity(), layout, items);
        listView.setAdapter(EventoAdapter);

    }

    public void RecogerDatos(){
        items = new ArrayList<EventoItem>();
        int id = -1;
        String nombre = null;


        //Abrimos la base de datos en modo lectura
        // Recibe el contexto y la ruta de la base de datos
        DatosBBDD usdbh =
                new DatosBBDD(getActivity(), Environment.getExternalStorageDirectory()+"/CulturalEvents.db", null, 1);

        // creamos la variable de la base de datos
        SQLiteDatabase db = usdbh.getReadableDatabase();

        // la sentencia SQL
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String idEvento = prefs.getString("id", "no existe id");
        String[] args = new String[]{idEvento};
        String selectSQL = "SELECT * FROM evento where id_acontecimiento=?";
        Cursor c = db.rawQuery(selectSQL, args);



            // Buscamos el layout principal para eliminar el textView
            layoutPrincipal = (FrameLayout) getActivity().findViewById(R.id.listado_fragment);

            while (c.moveToNext()) {
                id = c.getInt(c.getColumnIndex("id"));
                nombre = c.getString(c.getColumnIndex("nombre")); //recogemos los datos de la columna 'nombre' de la base de datos
                // Creamos el objeto
                EventoItem evento = new EventoItem(String.valueOf(id), nombre);
                items.add(evento);
            }
    }

    @Override
    public void onStart(){
        MyLog.d("listadoFragment", "onStart...");
        super.onStart();

        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.contenido_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        MyLog.d("listadoFragment", "onListItemClick...");
        // Notify the parent activity of selected item
        if (mListener != null) {
            mListener.onFragmentInteraction(position, items.get(position));
        }

        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int position, EventoItem item);
    }
}
