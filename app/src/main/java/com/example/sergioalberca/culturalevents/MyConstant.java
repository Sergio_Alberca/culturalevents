package com.example.sergioalberca.culturalevents;

/**
 * Created by Sergio Alberca on 04/10/2016.
 */

public class MyConstant {

    public static final float VIEWPORT_WIDTH = 5.0f;
    public static final String REST_URL = "http://www.ejemplo.com/api";
}
