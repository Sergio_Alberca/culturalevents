package com.example.sergioalberca.culturalevents;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMapAcontecimiento;
    private GoogleMap mMapEvento;
    private Context context;
    private String latitud;
    private String longitud;
    private String nombre;
    private String latitudevento;
    private String longitudevento;
    private String nombreevento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapAcontecimiento = googleMap;

        DatosBBDD creacionBD = new DatosBBDD(this.getApplicationContext(), Environment.getExternalStorageDirectory() + "/CulturalEvents.db", null, 1);
        //Le daremos permiso a la Base de Datos para que pueda leer
        SQLiteDatabase db = creacionBD.getReadableDatabase();

        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String idAcontecimiento = prefs.getString("id", "no existe id");
        String[] args = new String[]{idAcontecimiento};
        String sql = "Select nombre, latitud, longitud from acontecimiento where id=?";
        String sql2 = "Select nombre, latitud, longitud from evento where id_acontecimiento=?";
        Cursor c = db.rawQuery(sql, args);
        Cursor c2 = db.rawQuery(sql2,args);

        if (c.moveToFirst()) {
            nombre = c.getString(c.getColumnIndex("nombre")); //recogemos los datos de la columna 'nombre' de la base de datos
            longitud = c.getString(c.getColumnIndex("longitud"));
            latitud = c.getString(c.getColumnIndex("latitud"));

            // Añadimos un marcador al mapa de acontecimiento
            LatLng acontecimiento = new LatLng(Float.parseFloat(latitud), Float.parseFloat(longitud));
            mMapAcontecimiento.addMarker(new MarkerOptions().position(acontecimiento).title(nombre).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
            mMapAcontecimiento.moveCamera(CameraUpdateFactory.newLatLng(acontecimiento));
        }
        while (c2.moveToNext()){
            nombreevento = c2.getString(c2.getColumnIndex("nombre")); //recogemos los datos de la columna 'nombre' de la base de datos
            longitudevento = c2.getString(c2.getColumnIndex("longitud"));
            latitudevento = c2.getString(c2.getColumnIndex("latitud"));

            // Añadimos un marcador al mapa de evento
            LatLng evento = new LatLng(Float.parseFloat(latitudevento), Float.parseFloat(longitudevento));
            mMapAcontecimiento.addMarker(new MarkerOptions().position(evento).title(nombreevento).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            mMapAcontecimiento.moveCamera(CameraUpdateFactory.newLatLng(evento));
        }

        CameraUpdate camUpd1 =
                CameraUpdateFactory
                        .newLatLngZoom(new LatLng(Float.parseFloat(latitud), Float.parseFloat(longitud)), 13);

        mMapAcontecimiento.moveCamera(camUpd1);
    }
}
