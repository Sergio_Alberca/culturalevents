package com.example.sergioalberca.culturalevents;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class MostrarEventoFragment extends Fragment {

    int mCurrentPosition = -1;
    String id;
    private LinearLayout layoutPrincipal;

    public MostrarEventoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MyLog.d("MostrarEventoFragment", "onCreateView...");

        // Si la actividad se ha recreado (por ejemplo desde la rotación de la pantalla), se restaura
        // la selección de artículo anterior establecida por onSaveInstanceState ().
        // Esto es primordialmente necesario cuando está en el diseño de dos paneles.
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt("position");
            id = savedInstanceState.getString("id");
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mostrar_evento, container, false);
    }

    @Override
    public void onStart(){

        MyLog.d("contenidoFragment", "onStart...");
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateView(args.getInt("position"), args.getString("id"));
        } else if (mCurrentPosition != -1) {
            // Set article based on saved instance state defined during onCreateView
            updateView(mCurrentPosition, id);
        }
    }

    public void updateView(int position, String id){
        MyLog.d("contenidoFragment", "updateView...");
        crearLayout(id);

    }

    protected void crearLayout(String idEvento){
        String nombre = null;
        String descripcion = null;
        String inicio = null;
        String fin = null;
        String direccion = null;
        String localidad = null;
        String cod_postal = null;
        String provincia = null;
        String latitud = null;
        String longitud = null;

        //Abrimos la base de datos en modo lectura
        // Recibe el contexto y la ruta de la base de datos
        DatosBBDD usdbh =
                new DatosBBDD(getActivity(), Environment.getExternalStorageDirectory()+"/CulturalEvents.db", null, 1);

        // creamos la variable de la base de datos
        SQLiteDatabase db = usdbh.getReadableDatabase();

        // creamos un array de strings, en el cual introduciremos el campo que queremos recoger del where
        String[] args = new String[] {idEvento};
        // la sentencia SQL
        String selectSQL = "SELECT * FROM evento WHERE id=?";
        // creamos un cursor con la ejecución del select en la base de datos

        Cursor c = db.rawQuery(selectSQL, args);
        if (c.moveToFirst()) {
            nombre = c.getString(c.getColumnIndex("nombre")); //recogemos los datos de la columna 'nombre' de la base de datos
            descripcion = c.getString(c.getColumnIndex("descripcion"));
            inicio = c.getString(c.getColumnIndex("inicio"));
            fin = c.getString(c.getColumnIndex("fin"));
            direccion = c.getString(c.getColumnIndex("direccion"));
            localidad = c.getString(c.getColumnIndex("localidad"));
            cod_postal = c.getString(c.getColumnIndex("cod_postal"));
            provincia = c.getString(c.getColumnIndex("provincia"));
            latitud = c.getString(c.getColumnIndex("latitud"));
            longitud = c.getString(c.getColumnIndex("longitud"));

            try {
                // creamos el formatero de como lo recoge en la base de datos
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmm");
                Date fechaInicio = dateFormat.parse(inicio);
                Date fechaFin = dateFormat.parse(fin);

                // creamos el formato en el que lo va a mostrar
                SimpleDateFormat formatPrint = new SimpleDateFormat("d/M/y hh:mm");
                String inicioFormat = formatPrint.format(fechaInicio);
                String finFormat = formatPrint.format(fechaFin);

                //Añadimos el LinearLayout
                layoutPrincipal = (LinearLayout) getActivity().findViewById(R.id.fragment_layout_mostrar);

                layoutPrincipal.setOrientation(LinearLayout.VERTICAL);
                // Asignamos el scrollbar al layout
                //layoutPrincipal.scrollBy(0, 5);

                // Eliminamos todas las vistas del layout contenedor, para que cuando se vuelva a pulsar
                // en el evento no aparezcan dobles.
                layoutPrincipal.removeAllViewsInLayout();

                //Añadimos el TextView de nombre del acontecimiento. Y posteriormente hacemos lo mismo con todas las variables posteriores
                createTextView(R.drawable.nombre, nombre, layoutPrincipal);
                createTextView(R.drawable.descripcion, descripcion, layoutPrincipal);
                createTextView(R.drawable.coordenadas, inicioFormat, layoutPrincipal);
                createTextView(R.drawable.coordenadas, finFormat, layoutPrincipal);
                createTextView(R.drawable.localidad, direccion, layoutPrincipal);
                createTextView(R.drawable.localidad, localidad, layoutPrincipal);
                createTextView(R.drawable.email, cod_postal, layoutPrincipal);
                createTextView(R.drawable.localidad, provincia, layoutPrincipal);
                createTextView(R.drawable.coordenadas, latitud, layoutPrincipal);
                createTextView(R.drawable.coordenadas, longitud, layoutPrincipal);
            }catch (ParseException e){
                e.printStackTrace();
            }

        }
    }

    /**
     * Creamos un método que recibirá el valor del TextView, los params del layout y el tipo de layout.
     * @param valorTV
     * @param layoutPrincipal
     */
    protected void createTextView(int imgCod, String valorTV, LinearLayout layoutPrincipal) {
        if (!valorTV.isEmpty()) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            // creamos otro linear layout al cual le asignamos un estilo, después los parametros y la orientación
            LinearLayout layoutSecundario = new LinearLayout(new ContextThemeWrapper(getActivity(), R.style.AppTheme));
            layoutSecundario.setLayoutParams(params);
            layoutSecundario.setOrientation(LinearLayout.HORIZONTAL);

            // creamos el text view con el nombre que reciba
            TextView newTV = new TextView(new ContextThemeWrapper(getActivity(), R.style.AppTheme));
            // le asignamos el valor que reciva al tv
            newTV.setText(valorTV.toString());
            // le asignamos los parametros al tv
            newTV.setLayoutParams(params);

            newTV.setGravity(Gravity.CENTER);

            // creamos la imagen view, que será el icono
            ImageView iv = new ImageView(getActivity());
            // asignamos el codigo de imagen a la imagen (esto hará que encuentre la imagen y la muestre)
            iv.setImageResource(imgCod);

            layoutSecundario.addView(iv);
            layoutSecundario.addView(newTV);

            layoutPrincipal.addView(layoutSecundario);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        MyLog.d("contenidoFragment", "onSaveInstanceState...");
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putInt("position", mCurrentPosition);
        outState.putString("id", id);
    }
}
