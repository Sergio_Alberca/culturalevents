package com.example.sergioalberca.culturalevents;

/**
 * Created by Sergio Alberca on 19/01/2017.
 */

public class EventoItem {
    private String nombre;
    private String id;
    private String latitud;
    private String longitud;

    public EventoItem(String id, String nombre){
        this.nombre = nombre;
        this.id = id;
    }

    public EventoItem(String nombre, String latitud, String longitud){
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
    }
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
