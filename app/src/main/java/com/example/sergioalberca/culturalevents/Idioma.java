package com.example.sergioalberca.culturalevents;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import java.util.Locale;

/**
 * Created by Sergio Alberca on 09/02/2017.
 */

public class Idioma extends Activity{
    public static void CambiaIdioma(Activity activity){
        //Cargamos las preferencias de los ajustes de la aplicación.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activity);
        Resources res = activity.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(pref.getString("lenguaje","en"));
        res.updateConfiguration(conf, dm);
    }
}
