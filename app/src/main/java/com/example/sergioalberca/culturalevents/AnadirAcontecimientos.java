package com.example.sergioalberca.culturalevents;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Locale;

public class AnadirAcontecimientos extends AppCompatActivity {

    Button button_anadir;
    EditText editTextBuscar;
    public static Context mycontext;
    private final int REQUEST_CODE_INTERNET = 0;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Idioma.CambiaIdioma(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_acontecimientos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mycontext = this;

        editTextBuscar = (EditText) findViewById(R.id.editText_buscarid);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100);
        button_anadir = (Button) findViewById(R.id.button_anadir);
        button_anadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editTextBuscar.getWindowToken(), 0);
                if(!isOnline()) {

                }else{

                    String id = editTextBuscar.getText().toString();
                int longitud = id.length();
                if (longitud == 0) {
                    Snackbar.make(v, "Introduzca un id correcto", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    int permissionCheck = ContextCompat.checkSelfPermission(mycontext, Manifest.permission.INTERNET);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        progressBar.setProgress(25);
                        DescargarDatos dd = new DescargarDatos(id,mycontext,progressBar);
                        progressBar.setProgress(100);
                        dd.execute();
                    } else {
                        if (Build.VERSION.SDK_INT <= 23) {
                            // Explicar permiso
                            if (ActivityCompat.shouldShowRequestPermissionRationale(AnadirAcontecimientos.this, Manifest.permission.INTERNET)) {
                                Toast.makeText(AnadirAcontecimientos.this, "El permiso es necesario para conectarse a internet.",
                                        Toast.LENGTH_SHORT).show();
                                // Solicitar el permiso
                                ActivityCompat.requestPermissions(AnadirAcontecimientos.this, new String[]{Manifest.permission.INTERNET}, REQUEST_CODE_INTERNET);
                            }else{
                                Snackbar.make(v, "No hay conexion a internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }
                        }
                    }

                }

            }

        }}
        );
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(mycontext, "No hay conexion a internet", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

