package com.example.sergioalberca.culturalevents;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MostrarAcontecimientosActivity extends AppCompatActivity {
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Idioma.CambiaIdioma(this);
        super.onCreate(savedInstanceState);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_mostrar_acontecimientos);
        CrearContenedor();
        ComprobarEventos();
    }

    public void ComprobarEventos(){
        DatosBBDD creacionBD = new DatosBBDD(this.getApplicationContext(), Environment.getExternalStorageDirectory() + "/CulturalEvents.db", null, 1);
        //Le daremos permiso a la Base de Datos para que pueda leer
        SQLiteDatabase db = creacionBD.getReadableDatabase();

        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String idAcontecimiento = prefs.getString("id", "no existe id");
        String[] args = new String[]{idAcontecimiento};
        String sql = "Select * from evento where id_acontecimiento=?";
        final Cursor cu = db.rawQuery(sql, args);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cu.getCount() != 0){
                Intent intent = new Intent(MostrarAcontecimientosActivity.this, EventosActivity.class);
                startActivity(intent);
                }else{
                    Toast.makeText(MostrarAcontecimientosActivity.this, "El acontecimiento no tiene eventos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }



    /**
     * metodo para que funcione el boton atras y se valla ala actividad que nosotros deseemos.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this.getApplicationContext(), ListadoAcontecimientosActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);


    }

    public void CrearContenedor() {

        String nombre = null;
        String organizador = null;
        String descripcion = null;
        String tipo = null;
        String portada = null;
        String inicio = null;
        String fin = null;
        String direccion = null;
        String localidad = null;
        String cod_postal = null;
        String provincia = null;
        String latitud = null;
        String longitud = null;
        String telefono = null;
        String email = null;
        String web = null;
        String facebook = null;
        String twitter = null;
        String instagram = null;
        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_mostrar_acontecimientos);

        DatosBBDD creacionBD = new DatosBBDD(this.getApplicationContext(), Environment.getExternalStorageDirectory() + "/CulturalEvents.db", null, 1);
        //Le daremos permiso a la Base de Datos para que pueda leer
        SQLiteDatabase db = creacionBD.getReadableDatabase();

        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String idAcontecimiento = prefs.getString("id", "no existe id");
        String[] args = new String[]{idAcontecimiento};
        String sql = "Select * from acontecimiento where id=?";
        Cursor c = db.rawQuery(sql, args);
        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst() == true) {
            //Recorremos el cursor hasta que no haya más registros
            nombre = c.getString(c.getColumnIndex("nombre"));
            organizador = c.getString(c.getColumnIndex("organizador"));
            descripcion = c.getString(c.getColumnIndex("descripcion"));
            tipo = c.getString(c.getColumnIndex("tipo"));
            portada = c.getString(c.getColumnIndex("portada"));
            inicio = c.getString(c.getColumnIndex("inicio"));
            fin = c.getString(c.getColumnIndex("fin"));
            direccion = c.getString(c.getColumnIndex("direccion"));
            localidad = c.getString(c.getColumnIndex("localidad"));
            cod_postal = c.getString(c.getColumnIndex("cod_postal"));
            provincia = c.getString(c.getColumnIndex("provincia"));
            latitud = c.getString(c.getColumnIndex("latitud"));
            longitud = c.getString(c.getColumnIndex("longitud"));
            telefono = c.getString(c.getColumnIndex("telefono"));
            email = c.getString(c.getColumnIndex("email"));
            web = c.getString(c.getColumnIndex("web"));
            facebook = c.getString(c.getColumnIndex("facebook"));
            twitter = c.getString(c.getColumnIndex("twitter"));
            instagram = c.getString(c.getColumnIndex("instagram"));

            try {
                // creamos el formatero de como lo recoge en la base de datos
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmm");
                Date fechaInicio = dateFormat.parse(inicio);
                Date fechaFin = dateFormat.parse(fin);

                // creamos el formato en el que lo va a mostrar
                SimpleDateFormat formatPrint = new SimpleDateFormat("d/M/y hh:mm");
                String inicioFormat = formatPrint.format(fechaInicio);
                String finFormat = formatPrint.format(fechaFin);

                crearLayoutConImagen(linearLayout1, R.drawable.nombre, nombre);
                crearLayoutConImagen(linearLayout1, R.drawable.organizador, organizador);
                crearLayoutConImagen(linearLayout1, R.drawable.descripcion, descripcion);
                crearLayoutConImagen(linearLayout1, R.drawable.descripcion, tipo);
                crearLayoutConImagen(linearLayout1, R.drawable.portada, portada);
                crearLayoutConImagen(linearLayout1, R.drawable.coordenadas, inicioFormat);
                crearLayoutConImagen(linearLayout1, R.drawable.coordenadas, finFormat);
                crearLayoutConImagen(linearLayout1, R.drawable.direccion, direccion);
                crearLayoutConImagen(linearLayout1, R.drawable.localidad, localidad);
                crearLayoutConImagen(linearLayout1, R.drawable.localidad, cod_postal);
                crearLayoutConImagen(linearLayout1, R.drawable.localidad, provincia);
                crearLayoutConImagen(linearLayout1, R.drawable.coordenadas, latitud);
                crearLayoutConImagen(linearLayout1, R.drawable.coordenadas, longitud);
                crearLayoutConImagen(linearLayout1, R.drawable.telefono, telefono);
                crearLayoutConImagen(linearLayout1, R.drawable.email, email);
                crearLayoutConImagen(linearLayout1, R.drawable.web, web);
                crearLayoutConImagen(linearLayout1, R.drawable.facebbok, facebook);
                crearLayoutConImagen(linearLayout1, R.drawable.twitter, twitter);
                crearLayoutConImagen(linearLayout1, R.drawable.instagram, instagram);

                Button button_map = new Button(new ContextThemeWrapper(this, R.style.AppTheme));
                linearLayout1.addView(button_map);
                button_map.setText("Ver Mapa");
                final String finalLatitud = latitud;
                final String finalLongitud = longitud;
                button_map.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        if(finalLatitud.isEmpty() || finalLongitud.isEmpty()){
                            Toast.makeText(MostrarAcontecimientosActivity.this, "¡El acontecimiento no tiene coordenadas!", Toast.LENGTH_SHORT).show();

                        }else{

                            Intent intent = new Intent(MostrarAcontecimientosActivity.this, MapsActivity.class);
                            startActivity(intent);

                        }
                    }
                });
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            finish();
        }
    }



    void crearLayoutConImagen(LinearLayout ly, int res_img, String text) {
        if (!text.isEmpty()) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout contenedor = new LinearLayout(new ContextThemeWrapper(this, R.style.AppTheme));
        contenedor.setLayoutParams(params);
        contenedor.setOrientation(LinearLayout.HORIZONTAL);

        //Crear imagen
        ImageView valueIV = new ImageView(new ContextThemeWrapper(this, R.style.AppTheme));
        valueIV.setImageResource(res_img);

        //Crear texto
        TextView valueTV = new TextView(new ContextThemeWrapper(this, R.style.AppTheme));
        valueTV.setText(text.toString());
        valueTV.setLayoutParams(params);

        //Añadir elementos al LinearLayout contenedor
        contenedor.addView(valueIV);
        contenedor.addView(valueTV);

        //Añadir LinearLayout al principal creado en XML de la Actividad
        ly.addView(contenedor);
        }
    }

}


