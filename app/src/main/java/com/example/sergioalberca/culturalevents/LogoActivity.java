package com.example.sergioalberca.culturalevents;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class LogoActivity extends AppCompatActivity {

    private final static String Activity = "LogoActivity";
    SharedPreferences pref;

    // Duración en milisegundos que se mostrará el splash
    private final int DURACION_SPLASH = 3000; // 3 segundos

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Idioma.CambiaIdioma(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        RecogeUltAcontecimiento();
        //codigo para hacer que el splah screen dure 3 segundos y se vaya a la siguiente actividad
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(LogoActivity.this, ListadoAcontecimientosActivity.class);
                startActivity(intent);
                if(RecogeUltAcontecimiento() == true){
                    Intent intent2 = new Intent(LogoActivity.this, MostrarAcontecimientosActivity.class);
                    startActivity(intent2);
                }

                finish();
            }
        }, 3000);

    }

    public boolean RecogeUltAcontecimiento(){

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        return pref.getBoolean("switch_acontecimiento",false);
    }

    @Override
    protected void onStart() {
        MyLog.d(Activity, "OnStar...");
        super.onStart();

    }

    @Override
    protected void onResume() {
        MyLog.d(Activity, "OnResume...");
        super.onResume();
    }

    @Override
    protected void onPause() {
        MyLog.d(Activity, "OnPause...");
        super.onPause();
    }

    @Override
    protected void onStop() {
        MyLog.d(Activity, "OnStop...");
        super.onStop();

    }

    @Override
    protected void onRestart() {
        MyLog.d(Activity, "OnRestart...");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        MyLog.d(Activity, "OnDestroy...");
        super.onDestroy();
    }

    public  void CambiaIdioma(Activity activity){
        //Cargamos las preferencias de los ajustes de la aplicación.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activity);
        Resources res = activity.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(pref.getString("lenguaje","en"));
        res.updateConfiguration(conf, dm);
    }
}
